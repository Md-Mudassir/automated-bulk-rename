# Automated Bulk Rename

Dynamically make multiple copies of a file (docx, xlsx) with Python 3 and rename those copied files whose names are present in an Excel sheet.

### DEMO
![](https://i.ibb.co/0BWWYVF/ezgif-com-video-to-gif-1.gif)

### Dependencies:

- `openpyxl` : for extracting the names from excel sheet,
- `tkinter` : open GUI where user can select a template (docx) file and Excel sheet to automate the process.
- `shutil` : for efficient copy, rename process.
- `os` : to get the current working directory & to make a new directory.

### Windows Executable

Converted the python script to windows compatible executable (.exe) using `pyinstaller` & `auto-py-to-exe`.
