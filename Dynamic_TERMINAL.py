#! Python 3
#Copy a any file multiple times and rename it from a excel list.

import openpyxl
import os
import shutil

#File with file name data
file_names = openpyxl.load_workbook(input("Enter File name with ext : "))#Add the file name [INPUT]
file_names_sheet = file_names['Sheet1']#Add the sheet name

#Grab the file Template [INPUT]
template = input("Enter File Name to be Copied with Extension : ") 

#New Folder Name [INPUT]
folder = input("Enter Folder Name : ") 

#Naming Convention [INPUT]
name_convention=input("Enter naming : ") 

#Takes: start cell, end cell, and sheet you want to copy from.
def copyRange(startCol, startRow, endCol, endRow, sheet):
    rangeSelected = []
    #Loops through selected Rows
    for i in range(startRow,endRow + 1,1):
        #Appends the row to a RowSelected list
        rowSelected = []
        for j in range(startCol,endCol+1,1):
            rowSelected.append(sheet.cell(row = i, column = j).value)
        #Adds the RowSelected List and nests inside the rangeSelected
        rangeSelected.append(rowSelected)
 
    return rangeSelected

def createFiles():
    print('Processing...')

    #Make a foler for the files
    current_directory = os.getcwd()
    folder_n_path = os.path.join(current_directory,folder)
    print("Files saved to: "+folder_n_path)
    try:
        newFolder = os.makedirs(folder_n_path)
        
    except:
        print("Folder already exists")
        return

    #Get the Data to make the file names
    selectedRange = copyRange(1,2,2,11,file_names_sheet)
    print(selectedRange)
    #Loop through each row
    for i in selectedRange:
        # print (i[0]+" "+i[1]+" Q3 Grades 2017")
        file_name = i[0]+""+name_convention+ ".docx"
        
        #Combine the file path with the new file name.
        combined_file_path = os.path.join(folder,file_name)
        print(combined_file_path)
        shutil.copy(template, combined_file_path)
    print("Done")


go = createFiles()