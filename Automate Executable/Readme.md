## Automate Demonstration

### Points to be noted.
1. Make sure to keep the Automate.exe, docx, xlsx files in the same folder.
2. Identify number of names present in the Excel sheet excluding the Title (Firstname, Lastname), Ex: 5
3. Once you click on ```Godspeed``` Button the window will automatically close and it will generate a folder named ```Copied_Files``` in the same directory where all the files are present.
4. Before executing another task delete the ```Copied_Files``` folder, as it will not work if that folder is already exists.

### DEMO
![](https://i.ibb.co/0BWWYVF/ezgif-com-video-to-gif-1.gif)

#### [Demo Video Link](https://drive.google.com/file/d/1bhLkY5VXHESblk1C3Wt2JVwPcAG60yFB/view)